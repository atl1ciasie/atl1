<?php

namespace mygiftbox\view;

use mf\view\AbstractView;
use mf\router\Router;
use mygiftbox\auth\MygiftboxAuthentification;
use mygiftbox\model\Benefit;

class MygiftboxView extends AbstractView
{
    /* Constructeur
        *
        * Appelle le constructeur de la classe parent
         *
        */
    public function __construct( $data=null ){
        parent::__construct($data);
    }


    /** Méthode
     *
     * Selectionne les blocs de vues à afficher en fonction du nom donné en parametre
     *
     * @return mixed bloc html
     */
    protected function renderBody($selector=null)
    {
        $header = $this->renderHeader();
        $footer = $this->renderFooter();
        $user = new MygiftboxAuthentification();
        if($user->logged_in){
            $topMenu = $this->renderTopMenuLogin();
        }
        else{
            $topMenu = $this->renderTopMenu();
        }
        switch ($selector) {
            case 'categories':
                $main = $this->renderCategories();
                break;
            case 'benefits':
                $main = $this->renderBenefits();
                break;
            case 'signup':
                $main = $this->renderSignup();
                break;
            case 'benefitsCategory':
                $main = $this->renderBenefitsCategory();
                break;
            case'basket':
                $main = $this->renderBasket();
                break;
            case 'benefit':
                $main = $this->renderBenefit();
                break;
            case 'login':
                $main = $this->renderLogin();
                break;
            case 'home':
                $main = $this->renderHome();
                $header = $this->renderHeaderHome();
                $footer = $this->renderFooterHome();
                break;
            case 'url':
                $main = $this->renderURL();
                break;
            case 'message':
                $main = $this->renderMessage();
                break;
            case 'profil':
                $main = $this->renderProfil();
                break;
            case 'review':
                $main = $this->renderReview();
                break;
            case 'goReview':
                $main = $this->renderGoReview();
                break;
            case 'pay':
                $main = $this->renderPay();
                break;
            case 'offertList':
                $main = $this->renderOffertList();
                break;
            case 'offert':
                $main = $this->renderOffert();
                break;
            case 'messageOffer':
                $main = $this->renderMessageOffer();
                break;
            case 'error':
                $main = $this->renderError();
                break;
            case 'dateMessage':
                $main = $this->renderDateMessage();
                break;
        }


        $body = <<<EOT
            <header class='header'>
                ${header}
            </header>
            ${topMenu}
            <main class='container'>
                ${main}
            </main>
            </section>
            <footer class='footer'>
                ${footer}
            </footer>
EOT;
        return $body;
    }

    /** Méthode renderFooter
    * Affiche le footer
    * @return mixed bloc html
    */
    private function renderFooter(){
        return "<p>Plaisir d'offrir, joie de recevoir</p>";
    }

    /** Méthode renderHeaderHome
    * Affiche le header de la page home
    * @return mixed bloc html
    */
    private function renderHeaderHome(){

        $return = '<h1>MyGiftBox.app</h1>';
        $return .= "<p>Plaisir d'offrir, joie de recevoir</p>";
        return $return;
    }
    /** Méthode renderFooterHome
     * Affiche le footer de la page home
     * @return mixed bloc html
     */
    private function renderFooterHome(){
        $return = "<div id='footerHome'>";
        $return .= "<div id='contact' class='blocFooterHome'>";
        $return .= '<h3>Nous contacter</h3>';
        $return .= "<p>2 ter, boulevard Charlemagne</p>";
        $return .= "<p>54000 Nancy</p>";
        $return .= "<p>atelier.un@mail.fr</p>";
        //$return .= '<a href="#"><i class="fas fa-envelope"></i></a>';
        $return .= "</div>";

        $return .= "<div id='reseaux' class='blocFooterHome'>";
        $return .= '<h3>Réseaux sociaux</h3>';
        $return .= "<div class='icones'>";
        $return .= '<a href="#"><i class="fab fa-twitter"></i></a>';
        $return .= '<a href="#"><i class="fab fa-facebook-square"></i></a>';
        $return .= '<a href="#"><i class="fab fa-instagram"></i></a>';
        $return .= "</div>";
        $return .= "</div>";

        $return .= "<div id='paiement' class='blocFooterHome'>";
        $return .= '<h3>Moyens de paiement</h3>';
        $return .= "<div class='icones'>";
        $return .= '<a href="#"><i class="fab fa-cc-mastercard"></i></a>';
        $return .= '<a href="#"><i class="fab fa-cc-visa"></i></a>';
        $return .= '<a href="#"><i class="fab fa-paypal"></i></a>';
        $return .= "</div>";
        $return .= "</div>";
        $return .= "</div>";

        return $return;
    }

    /** Méthode renderHeader
     * Affiche le header
     * @return mixed bloc html
     */
    private function renderHeader(){

        $return = '<h1>MyGiftBox.app</h1>';
        return $return;
    }

    /** Méthode renderHeader
     * Affiche le menu d'une personne déconnecté
     * 
     * @return mixed bloc html
     */
    private function renderTopMenu(){
        $router = new Router();

        $return = "<nav id='navUnlogin'>";
        $return .= "<a href='".$router->urlFor('home', array())."'><i class='fas fa-home'></i></a>";
        $return .= "<a href='".$router->urlFor('login', array())."'><i class='fas fa-user'></i></a>";
        $return .= "<a href=''><i class='fas fa-shopping-cart'></i></a>";
        $return .= "</nav>";
        return $return;
    }

    /** Méthode renderTopMenuLogin
     * Affiche le menu d'une personne connecté
     * @return mixed bloc html
      */
    private function renderTopMenuLogin(){
        $router = new Router();

        $return = "<nav id='navLogin'>";
        $return .= "<a href='".$router->urlFor('home', array())."'><i class='fas fa-home'></i></a>";
        $return .= "<a href='".$router->urlFor('profil', array())."'><i class='fas fa-user'></i></a>";/* Changer le nom de la route poure profil */
        $return .= "<a href='".$router->urlFor('logout', array())."'><i class='fas fa-sign-out-alt'></i></a>";
        $return .= "<a href='".$router->urlFor('basket', array())."'><i class='fas fa-shopping-cart'></i></a>";
        $return .= "</nav>";
        return $return;
    }

    /** Méthode renderBenefits
     *
     * Affiche toutes les prestations
     * 
     * @return mixed bloc html
     */
    private function renderBenefits(){
        $router = new Router();

        $benefits = $this->data;
        $return = '<section id="benefits">';
        $return .= '<h2> Prestations </h2>';
        $return .= "<span class='tri'>Tri par prix : ";
        $return .= "<a href='".$router->urlFor('benefits', array(array('tri', 'ASC')))."'>Croissant</a> ";
        $return .= "<a href='".$router->urlFor('benefits', array(array('tri', 'DESC')))."'>Décroissant</a>";
        $return .= "</span>";
        $return .= "<a href='".$router->urlFor('categories', array())."' class='button'>Categories</a>";
        foreach ($benefits as $benefit) {
            $category = $benefit->category;// Pour récupérer la catégorie de la prestation
            $return .= "<article class='benefit-container'>";
            $return .= "<img  src='" . Router::$image_path . $benefit->image . "' alt='" . $benefit->nom . "'>";
            $return .= "<h3><a href='".$router->urlFor('benefit', array(array('id', $benefit->id)))."'>" . $benefit->nom . "</a></h3>";
            $return .= "<em>" . $category->nom . "</em>";
            $return .= "<span class='priceBenefits'>" . $benefit->prix . " €</span>";
            $return .= "</article>";
        }
        $return .= "</section>";
        return $return;
    }

    /** Méthode renderCategories
     * Affichage de la liste des catégories
     * 
     * @return mixed bloc html
      */
    private function renderCategories(){
        $router = new Router();

        $return = "<section id='categories'>";
        foreach ($this->data as $category) {
            $return .= "<article class='categorie-container'>";
            $return .= "<h3><a href='".$router->urlFor('benefitsCategory', array(array('category', $category->id)))."'>$category->nom</a></h3>";
            $return .= "</article>";
        }
        $return .= "</section>";
        return $return;
    }


    /* Méthode renderSignup
     *
     * Vue de la fonctionalité pour créer un compte utilisateur
     * @return mixed bloc html
     */
    private function renderSignup(){
        $router = new Router();
        $urlForSendSignup = $router->urlFor('sendSignup');
        $return = '<section id="signup">';
        $return .= '<h2> Créer un compte </h2>';
        $return .= '<form class="forms" action="' . $urlForSendSignup . '" method=post>';
        $return .= '<input type=text name=name placeholder="Nom">';
        $return .= '<input type=text name=firstname placeholder="Prenom">';
        $return .= '<input type=email name=email placeholder="Email">';
        $return .= '<input type=password name=password placeholder="Password">';
        $return .= '<input type=password name=password_verify placeholder="Confirm Password">';
        $return .= '<button name=login_button type="submit" class="button">Création</button>';

        $return .= '</form>';
        $return .= '</section>';
        return $return;
    }

    /** Méthode renderBenefitsCategory
     * Affichage de la liste des prestation en fonction de la catégorie
     *
     * @return mixed bloc html
      */
    private function renderBenefitsCategory(){
        $router = new Router();
        $category = $this->data[0];
        $benefits = $this->data[1];
        $return = "<section id='benefits'>";
        $return .= "<h2>$category->nom</h2>";
        $return .= "<span class='tri'>Tri par prix : ";
        $return .= "<a href='".$router->urlFor('benefitsCategory', array(array('category', $category->id), array('tri', 'ASC')))."'>Croissant</a> ";
        $return .= "<a href='".$router->urlFor('benefitsCategory', array(array('category', $category->id), array('tri', 'DESC')))."'>Décroissant</a>";
        $return .= "</span>";
        foreach ($benefits as $benefit) {
            $return .= "<article class='benefit-container'>";
            $return .= "<img src='". Router::$image_path ."$benefit->image' alt='$benefit->nom'/>";
            $return .= "<h3><a href='".$router->urlFor('benefit', array(array('id', $benefit->id)))."'>$benefit->nom</a></h3>";
            $return .= "<span class='priceCategories'>$benefit->prix €</span>";
            $return .= "</article>";
        }
        $return .= "</section>";
        return $return;
    }

    /** Méthode renderBasket
     * Affichage du panier
     * @return mixed bloc html
      */
    private function renderBasket(){
        $benefits = $this->data[0];
        $nbBenefits = $this->data[1];

        $router = new Router();
        $urlForSaveBasket = $router->urlFor('saveBasket');
        $urlForBasketMessage = $router->urlFor('basketMessage');
        $urlForRemoveBasket = $router->urlFor('removeBasket');
        $urlForBenefits = $router->urlFor('benefits');

        $totalPrice = 0;
        $return = '<section id="basket">';
        $return .= '<h2> Mon coffret </h2>';
        if(!empty($benefits)){
            $i = 0;
            foreach ($benefits as $benefit) {
                $price = $benefit->prix * $nbBenefits[$i];
                $return .= "<article>";
                $return .= "<span>$benefit->nom</span>";
                $return .= "<span>$price €</span>";
                $return .= "<span>".$nbBenefits[$i]."</span>";
                $return .= "<form method='post' action='$urlForRemoveBasket'>";/* Mettre url de remove */
                $return .= "<input type='hidden' name='idBenefit' value='$benefit->id'/>";
                $return .= "<button class='button' name='removeButton' type='submit'><i class='fas fa-trash-alt'></i></button>";
                $return .= "</form>";
                $return .= "</article>";
                $totalPrice += $price;
                $i++;
            }
    
            $return .= '<div class="total">';
            $return .= 'Total: ' . $totalPrice . '€';
            $return .= '</div>';
            $return .= '<a href="'. $urlForBenefits . '">';
            $return .= '<i class="fas fa-plus-circle"></i>';
            $return .= '</a>';
            $return .= "<a href='".$urlForSaveBasket."' class ='button'>Sauvegarder</a> ";
            $return .= "<a href='".$urlForBasketMessage."' class ='button' >Valider</a> ";
        }
        else{
            $return .= '<p>Votre panier est vide</p>';
            $return .= '<a href="'. $urlForBenefits . '"';
            $return .= '<i class="fas fa-plus-circle"></i>';
            $return .= '</a>';
        }
        
        $return .= '</section>';
        return $return;
    }


    /** Méthode renderBenefit
     *
     * Affiche une prestation
     * 
     * @return mixed bloc html
     */
    private function renderBenefit(){
        $router = new Router();
        $benefit = $this->data;
        $idcategorie = $benefit->idCategorie;
        $category = $benefit->nom;
        $return = '<section id="benefit">';
        $return .= '<article>';
        $return .= '<img  src="' . Router::$image_path . $benefit->image . '" alt="' . $benefit->nom . '">';
        $return .= '<h3>' . $benefit->nom . '</h3>';
        $return .= '<em>'.$benefit->lieu.'</em>';
        $return .= '<span class="priceBenefits">' . $benefit->prix . ' €</span>';
        $return .= '<p>' . $benefit->description . '</p>';
        $return .= '<form method="post" action="'.$router->urlFor('addBasket', array()).'">'; /* Ajouter la route d'ajout à au panier */
        $return .= '<input type="hidden" name="idBenefit" value="'.$benefit->id.'" />';
        $return .= '<button class="button" name="btAdd" type="submit"><i class="fas fa-plus-circle"></i></button>';
        $return .= '</form>';
        $return .= '</article>';
        $return .= '</section>';
        return $return;
    }

    /** Méthode renderLogin
     * Affichage du formulaire d'authentification
     * 
     * @return mixed bloc html
      */
    private function renderLogin(){
        $router = new Router();
        
        $return = "<section id='login'>";
        $return .= "<h2>S'identifier</h2>";
        $return .= "<form class='forms' action='".$router->urlFor('sendLogin', array())."' method=post>";
        $return .= "<i class='fas fa-envelope'></i>";
        $return .= "<input class='forms-text' type='email' name='txtEmail' placeholder='Email'>";
        $return .= "<i class='fas fa-lock'></i>";
        $return .= "<input class='forms-text' type='password' name='txtPassword' placeholder='Password' >";
        $return .= "<a href='".$router->urlFor('signup', array())."'>Créer un compte</a>";
        $return .= "<button class='button' type='submit' name='btSubmit'>Confirmer</button>";
        $return .= "</form>";
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderHome
     *
     * Affiche la page d'accueil ainsi que les 6 prestations les plus récentes
     * @return mixed bloc html
     */
    private function renderHome(){
        $router = new Router();
        $benefits = $this->data;
        $urlForBenefits = $router->urlFor('benefits');
        $return = "<div id='home'>";
        $return .= "<section id='presentation'>";
        $return .= "<h2>Créez votre coffret sur mesure</h2>";
        $return .= "<h3>C'est vous qui choisissez !</h3>";
        $return .= "<article id='explication'>";
        $return .= "<ul>";
        $return .= "<li><span class = 'numero'>1</span>Définissez votre budget</li>";
        $return .= "<li><span class = 'numero' >2</span>Faites votre sélection d'article</li>";
        $return .= "<li><span class = 'numero'>3</span>Personalisez votre coffret avec un message personnel</li>";
        $return .= "</ul>";
        $return .= "</article>";
        $return .= "<a href='" . $urlForBenefits . "' class='button'>Créez un coffret</a>";
        $return .= "</section>";
        $return .= "<section id='newBenefits'>";
        $return .= "<h2>Nos nouvelles activités</h2>";
        foreach ($benefits as $benefit) {
            $urlForBenefit = $router->urlFor('benefit',[['id',$benefit->id]]);
            $return .= "<article class='newBenefit'>";
            $return .= '<img src="'. Router::$image_path . $benefit->image . '" alt="' . $benefit->nom . '"/>';
            $return .= "<a href='" . $urlForBenefit . "'><h3>". $benefit->nom . "</h3></a>";
            $return .= "<span class='categorie'>" . $benefit->category->nom . "</span>";
            $return .= "</article>";
        }
        $return .= "</section>";
        $return .= "</div>";

        return $return;
    }

    /** Méthode renderURL
     * Affichage de la génération de l'url
     * @return mixed bloc html
      */
    private function renderURL(){
        $router = new Router();
        $urlForUrl = $router->urlFor('basketUrl');
        $basket = $this->data;
        $path = str_replace($_SERVER['PATH_INFO'],'',$_SERVER['REQUEST_URI']);
        $url = "";
        if(!empty($basket->url)){
            $url = $_SERVER['HTTP_HOST'].$path."/url/?url=".$basket->url;
        }
        $return = "<section id='generateUrl'>";
        $return .= "<h2> Commande n° $basket->id</h2>";
        $return .= "<form method='post' action='$urlForUrl'>";
        $return .= "<input type='hidden' name='idBasket' value='$basket->id'/>";
        $return .= "<button class='button' type='submit' name='btSubmit'>Générer l'URL</button>";
        $return .= "</form>";
        $return .= "<div>$url</div>";
        $return .= "</section>";
        return $return;
    }

    /** Méthode renderMessage
     *
     * Affiche un formulaire d'identification pour le créateur du coffret
     * 
     */
    private function renderMessage(){
        $router = new Router();
        $command = $this->data;
        $urlForSendMessage = $router->urlFor('sendMessage');/* Etape suivante enregistrement */
        $returnBasket = $router->urlFor('basketReturn');/* Retour en arriere */
        $return = "<section id='validate'>";
        $return .= "<h2>Validation</h2>";
        $return .= "<form method='post' action='".$urlForSendMessage."'>";
        $return .= "<input type='hidden' name='idBasket' value='$command->id'/>";
        $return .= "<input type='text' name='name' placeholder='Nom' value='".$command->creator->nom."' />";
        $return .= "<input type='text' name='firstName' placeholder='Prenom' value='".$command->creator->prenom."' />";
        $return .= "<input type='email' name='mail' placeholder='Email' value='".$command->creator->mail."' />";
        $return .= "<label from='date'>Date d'accès :</label>";
        $return .= "<input type='date' name='date' placeholder='date' data-date='' data-date-format='DD MMMM YYYY' value='".date('d/m/Y')."'/>";
        $return .= "<textarea rows='4' cols='50' name='message' placeholder='Message'>";
        $return .= $command->messageCreateur;
        $return .= "</textarea>";
        $return .= "<button class='button' type='submit' name='btSubmit'>SUIVANT</button>";
        $return .= "</form>";
        $return .= "<form method='post' action='".$returnBasket."'>";
        $return .= "<input type='hidden' name='idBasket' value='$command->id'/>";
        $return .= "<button class='button' type='submit' name='modifier'>MODIFIER</button>";
        $return .= "</form>";
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderProfil
     * Affiche le profil d'une personne
     * @return mixed bloc html
      */
    private function renderProfil(){
        $router = new Router();
        $user = $this->data[0];
        $commands = $this->data[1];
        $return = '<section id="profil">';
        $return .= '<h2>Mon profil</h2>';
        $return .= '<article>';
        $return .= '<span>';
        $return .= '<strong>Nom : </strong>';
        $return .= '<span>'.$user->nom.'</span>';
        $return .= '</span>';
        $return .= '<span>';
        $return .= '<strong>Prenom : </strong>';
        $return .= '<span>'.$user->prenom.'</span>';
        $return .= '</span>';
        $return .= '<span>';
        $return .= '<strong>Mail : </strong>';
        $return .= '<span>'.$user->mail.'</span>';
        $return .= '</span>';
        $return .= '</article>';
        $return .= '<h3>Mes coffrets</h3>';
        $return .= '<article class="liste-coffrets">';
        foreach($commands as $command){
            $return .= '<span>';
            switch($command->idEtat){
                case 1:
                    /* Commande sauvegardé */
                    $urlForGenerate = $router->urlFor('basketReturn');
                    $return .= '<form method="post" action="'.$urlForGenerate.'">';
                    $return .= '<input type="hidden" name="id" value="'.$command->id.'"/>';
                    $return .= '<button class="button" type="submit" name="bt">';
                    $return .= 'Coffret ' . $command->id . ' : ' . $command->etat->label;
                    $return .= '</button>';
                    $return .= '</form>';
                break;
                case 2:
                    /* Commande validé */
                    $urlForGenerate = $router->urlFor('basketMessage');
                    $return .= '<form method="post" action="'.$urlForGenerate.'">';
                    $return .= '<input type="hidden" name="idBasket" value="'.$command->id.'"/>';
                    $return .= '<button class="button" type="submit" name="bt">';
                    $return .= 'Coffret ' . $command->id . ' : ' . $command->etat->label;
                    $return .= '</button>';
                    $return .= '</form>';
                break;
                case 3:
                    /* Commande Payé */
                    $urlForGenerate = $router->urlFor('basketUrl');
                    $return .= '<form method="post" action="'.$urlForGenerate.'">';
                    $return .= '<input type="hidden" name="idBasket" value="'.$command->id.'"/>';
                    $return .= '<button class="button" type="submit" name="bt">';
                    $return .= 'Coffret ' . $command->id . ' : ' . $command->etat->label;
                    $return .= '</button>';
                    $return .= '</form>';
                break;
                case 5:
                    /* Commande ouverte par le destinataire */
                    $urlForGenerate = $router->urlFor('messageOffer');
                    $return .= '<form method="post" action="'.$urlForGenerate.'">';
                    $return .= '<input type="hidden" name="idBasket" value="'.$command->id.'"/>';
                    $return .= '<button class="button" type="submit" name="bt">';
                    $return .= 'Coffret ' . $command->id . ' : ' . $command->etat->label;
                    $return .= '</button>';
                    $return .= '</form>';
                break;
                default:
                $return .= 'Coffret ' . $command->id . ' : ' . $command->etat->label;
                break;
            }
            $return .= '</span>';
        }
        $return .= '</article>';
        $return .= '</section>';
        return $return;
    }
    /** Méthode renderReview
     * Affichage du récapitulatif de la commande
     * @return mixed bloc html
      */
    private function renderReview(){
        $router = new Router();
        $urlForPay = $router->urlFor('viewPay');
        $urlForBasketMessage = $router->urlFor('basketMessage');
        $basket = $this->data;

        $return = "<section id='review'>";
        $return .= "<h2>Récapitulatif</h2>";
        $return .= "<article>";
        $return .= "<span><b>Nom : </b><span>".$basket->nomMsg."</span></span>";
        $return .= "<span><b>Prenom : </b><span>".$basket->prenomMsg."</span></span>";
        $return .= "<span><b>Mail : </b><span>".$basket->emailMsg."</span></span>";
        $return .= "<span><b>Mon message : </b></span>";
        $return .= "<p>$basket->messageCreateur</p>";
        $return .= "<span ><b>Mon coffret : </b></span>";
        $return .= "<div id='reviewBasket'>";
        $totalPrice = 0;
        foreach($basket->benefits as $benefit){
            $return .= "<span>";
            $return .= "<span>$benefit->nom</span>";
            $price = 0;
            foreach($basket->listeAchats as $achat){
                if($achat->idPrestation == $benefit->id){
                    $price = $achat->quantite * $benefit->prix;
                }
            }
            $totalPrice += $price;
            $return .= "<span> $price €</span>";
            $return .= "</span>";
        }
        $return .= "</div>";
        $return .= "<span><b>Mode de paiement classique</b></span>";
        $return .= "<span><b>Total : </b><span>$totalPrice €</span></span>";
        $return .= "</article>";
        $return .= "<form method='post' action='$urlForPay'>";/* Aller au paiement */
        $return .= "<input type='hidden' name='idBasket' value='$basket->id'/>";
        $return .= "<button class='button' type='submit' name='btSubmit'>VALIDER ET PAYER</button>";
        $return .= "<a href='$urlForBasketMessage' class='button'>MODIFIER</a>";
        $return .= "</form>";
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderGoReview
     * Renvoi vers la vue review
     * @return mixed bloc html
      */
    private function renderGoReview(){
        $router = new Router();
        $basket = $this->data;
        $urlForReview = $router->urlFor('reviewBasket', array(array('idBasket',$basket->id)));
        return "<meta http-equiv='refresh' content='0;URL=".$urlForReview."'>";
    }

    /** Méthode renderPay
     * Affiche le formulaire de paiement
     * @return mixed bloc html
      */
    private function renderPay(){
        $router = new Router();
        $urlForSend = $router->urlFor('sendPay');
        $basket = $this->data;

        $return = "<section id='pay'>";
        $return .= "<h2>Paiement</h2>";
        $return .= "<form method='post' action='$urlForSend'>";/* Mettre la route sendPay */
        $return .= "<input type='hidden' name='idBasket' value='$basket->id'/>";
        $return .= "<input type='text' name='name' placeholder='Tilulaire de la carte'/>";
        $return .= "<input type='text' name='card' placeholder='Numéro de la carte'/>";
        $return .= "<input class='date' type='text' name='month' placeholder='MM'/>";
        $return .= "<h3 class='date'> / </h3>";
        $return .= "<input class='date' type='text' name='year' placeholder='AAAA'/>";
        $return .= "<input type='text' name='ccv' placeholder='CCV'/>";
        $return .= "<button class='button' type='submit' name='btSubmit'>Payer</button>";
        $return .= "</form>";
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderOffert
     * Affiche un cadeau (mode séquence) pour le destinataire du coffret en ouvrant l'URL
     * @return mixed bloc html
     */
    private function renderOffert(){
        $router = new Router();
        $command = $this->data[0];
        $slide = $this->data[1];
        $url = $this->data[2];
        $urlForHome = $router->urlFor('home');
        $urlForSend = $router->urlFor('sendMessageOffert');
        $urlForOffertList = $router->urlFor('urlOffert', array(array('url',$url)));
        $urlForOffertSlide = $router->urlFor('urlOffert', array(array('url',$url),array('slide','0')));
        $next = $slide + 1;
        $urlForOffertSlideNext = $router->urlFor('urlOffert', array(array('url',$url),array('slide',$next)));
        $return = "<section id='offert'>";
        $return .= "<h2>Vos cadeaux !</h2>";
        $return .= "<div class='iconesOffert'><a href='$urlForOffertList'><i class='fas fa-th-list'></i></a>";
        $return .= "<a href='$urlForOffertSlide'><i class='fas fa-square'></i></a></div>";
        if(isset($command->benefits[$slide])){
            $benefit = $command->benefits[$slide];
            $return .= "<article>";
            $return .= "<img src='".Router::$image_path."$benefit->image' alt='$benefit->nom'/>";
            $return .= "<h3>$benefit->nom</h3>";
            $return .= '<em>'.$benefit->lieu.'</em>';
            $return .= '<p>' . $benefit->description . '</p>';
            $return .= "</article>";
            $return .= "<div>";
            $return .= "<h2>$command->prenomMsg $command->nomMsg</h2>";
            $return .= "<p>$command->messageCreateur</p>";
            $return .= "</div>";
            $return .= "<a href='$urlForOffertSlideNext' class='button'>Cadeau suivant</a>";
            $return .= "<form method='post' action='$urlForSend'>";
            $return .= "<input type='hidden' name='idBasket' value='$benefit->id'/>";
            $return .= "<textarea rows='4' cols='50' name='message' placeholder='Dites merci !'></textarea>";
            $return .= "<button type='submit' class='button' name='btSubmit'>Envoyer</button>";
            $return .= "</form>";
        }
        else{
            $return .= "<h3>Il n'y a plus de cadeaux</h3>";
            $return .= "<a href='$urlForHome' class='button'>Aller a la page d'accueil</a>";
        }
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderOffertList
     * Affiche un cadeau (mode liste) pour le destinataire du coffret en ouvrant l'URL
     * @return mixed bloc html
     */
    private function renderOffertList(){
        $router = new Router();
        $command = $this->data[0];
        $url = $this->data[1];
        $urlForSend = $router->urlFor('sendMessageOffert');
        $urlForOffertList = $router->urlFor('urlOffert', array(array('url',$url)));
        $urlForOffertSlide = $router->urlFor('urlOffert', array(array('url',$url),array('slide','0')));
        $return = "<section id='offertList'>";
        $return .= "<h2>Vos cadeaux !</h2>";
        $return .= "<div class='iconesOffert'><a href='$urlForOffertList'><i class='fas fa-th-list'></i></a>";
        $return .= "<a href='$urlForOffertSlide'><i class='fas fa-square'></i></a></div>";
        foreach($command->benefits as $benefit){
            $return .= "<article>";
            $return .= "<img src='".Router::$image_path."$benefit->image' alt='$benefit->nom'/>";
            $return .= "<h3>$benefit->nom</h3>";
            $return .= '<em>'.$benefit->lieu.'</em>';
            $return .= '<p>' . $benefit->description . '</p>';
            $return .= "</article>";
        }
        $return .= "<div>";
        $return .= "<h2>$command->prenomMsg $command->nomMsg</h2>";
        $return .= "<p>$command->messageCreateur</p>";
        $return .= "</div>";
        $return .= "<form method='post' action='$urlForSend'>";
        $return .= "<input type='hidden' name='idBasket' value='$benefit->id'/>";
        $return .= "<textarea rows='4' cols='50' name='message' placeholder='Dites merci !'></textarea>";
        $return .= "<button type='submit' class='button' name='btSubmit'>Envoyer</button>";
        $return .= "</form>";
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderMessageOffer
     * Affichage du message du destinataire pour le créateur
     * @return mixed bloc html
      */
    private function renderMessageOffer(){
        $command = $this->data;
        $return = "<section>";
        $return .= "<h2>Message du destinataire</h2>";
        $return .= "<p>$command->messageDestinataire</p>";
        $return .= "</section>";

        return $return;
    }

    /** Méthode renderDateMessage
     * Affiche la date a laquel le destinataire peut ouvrir le coffret
     * @return mixed bloc html
      */
    private function renderDateMessage(){
        $message = $this->data;

        $router = new Router();
        $urlForHome = $router->urlFor('home');

        $return = "<p>$message</p>";
        $return .= "<a href='$urlForHome' class='button'>Aller à la page d'accueil</a>";

        return $return;
    }

    /** Méthode renderError
     * Affichage des erreur
     * @param array tableau d'erreur
     * @return mixed bloc html
      */
    private function renderError(){
        $return = "<div id='error'>";
        $return .= $this->data;
        $return .= "</div>";

        $router = new Router();
        $urlForHome = $router->urlFor('home');
        $return .= "<meta http-equiv='refresh' content='2;URL=".$urlForHome."'>";

        return $return;
    }
}