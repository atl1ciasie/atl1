<?php
namespace mygiftbox\model;

class Benefit extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function category(){
        return $this->belongsTo('\mygiftbox\model\Category', 'idCategorie');
        /* Category : nom de la classe liée */
        /* idCategorie : nom de la clé étrangère courante */
    }
}
