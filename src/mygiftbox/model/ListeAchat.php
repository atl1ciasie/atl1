<?php
namespace mygiftbox\model;


class ListeAchat extends \Illuminate\Database\Eloquent\Model
{
    protected $table        = 'listeachat';  /* le nom de la table */
    protected $primaryKey   = 'id';     /* le nom de la clé primaire */
    public    $timestamps   = false;

}