<?php
namespace mygiftbox\model;

class Category extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function benefits(){
        return $this->hasMany('Prestation', 'idCategorie');
        /* Prestation : nom de la classe liée */
        /* idCategorie : nom de la clé étrangère liée */
    }
}