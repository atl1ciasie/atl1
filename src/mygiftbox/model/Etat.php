<?php
namespace mygiftbox\model;


class Etat extends \Illuminate\Database\Eloquent\Model
{
    protected $table        = 'etat';  /* le nom de la table */
    protected $primaryKey   = 'id';     /* le nom de la clé primaire */
    public    $timestamps   = false;

}