<?php
namespace mygiftbox\model;


class Command extends \Illuminate\Database\Eloquent\Model
{
    protected $table        = 'commande';  /* le nom de la table */
    protected $primaryKey   = 'id';     /* le nom de la clé primaire */
    public    $timestamps   = false;

    public function benefits () {/* retourne les prestations du coffret(de la commande)*/
        return $this->belongsToMany('mygiftbox\model\Benefit', 'listeachat', 'idCommande', 'idPrestation');

    }
    public function listeAchats () {/* retourne les ligne de la commande)*/
        return $this->hasMany('mygiftbox\model\ListeAchat', 'idCommande');
        /* Commande : nom de la classe liée */
        /* idUtilisateur : nom de la clé étrangère de la table liée */
    }

    public function creator(){/* retourne le créateur de la commande)*/
        return $this->belongsTo('\mygiftbox\model\User', 'idUtilisateur');
        /* User : nom de la classe liée */
        /* idUtilisateur : nom de la clé étrangère courante */
    }

    public function etat () {/* retourne le statut d'un coffret)*/
        return $this->belongsTo('mygiftbox\model\Etat', 'idEtat');

    }

    public function price () {/* retourne le prix total d'un coffret)*/
        $price = 0;
        foreach ($this->benefits as $benefit){
            $price += $benefit->prix;
        }
        return $price;
        
    }

}