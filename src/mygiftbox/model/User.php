<?php
namespace mygiftbox\model;

class User extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'utilisateur'; /* le nom de la table */
    protected $primaryKey = 'id'; /* le nom de la clé primaire */
    public $timestamps = false;

    public function commands(){
        return $this->hasMany('Commande', 'idUtilisateur');
        /* Commande : nom de la classe liée */
        /* idUtilisateur : nom de la clé étrangère de la table liée */
    }

    /** Méthode commandsParEtat
     *
     * pour renvoyer les commandes correspondant à un statut particulier
     *
     * @param $status: état de la commande que l'on recherche
     * @return mixed: les commandes correspondant à l'état recherché
     */
    public function commandsByStatus($status){
        $key = $this->primaryKey;
        $idUser = $this->$key;
        $commands = Command::where('idUtilisateur', '=', $idUser)
                ->where('idEtat', '=', $status)
                ->get();
        return $commands;

    }
}