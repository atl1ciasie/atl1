<?php
namespace mygiftbox\auth;

use mf\auth\Authentification;
use mygiftbox\model\User;
use mf\auth\exception\AuthentificationException;

class MygiftboxAuthentification extends Authentification {

    /* niveaux d'accès de Mygiftbox 
     *
     * Le niveau USER correspond a un utilisateur inscrit avec un compte
     * 
     * niveau NONE un utilisateur non inscrit est hérité 
     * depuis AbstractAuthentification 
     */
    const ACCESS_LEVEL_USER  = 100;

    /* constructeur */
    public function __construct(){
        parent::__construct();
    }

    /** Méthode loginUser
     * Permet de connecter un utilisateur
     * 
     * @return bool True si l'utilisateur est connecté, Flase s'il n'est pas connecté
      */
    public function loginUser($email, $password){
        $requete = User::where('mail', '=', $email);
        $user = $requete->first();
        if(!empty($user)){
            return $this->login($email, $user->password, $password, $user->level);
        }
        else{
            throw new AuthentificationException("L'utilisateur est inconnu");
        }
    }

    /** Méthode createUser
     *
     * Permet la création d'un nouvel utilisateur de l'application
     *
     * @param $name : le nom de l'utilisateur
     * @param $firstname : le prénom de l'utilisateur
     * @param $email : l'email de l'utilisateur qui lui servira aussi de login
     * @param $pass  le mot de passe choisi
     * @param int $level : le niveaux d'accès (par défaut ACCESS_LEVEL_USER)
     * @throws AuthentificationException
     */

    public function createUser($name, $firstname, $email, $pass, $level=self::ACCESS_LEVEL_USER)
    {
        $userVerif = User::select('id')->where('mail', '=', $email)->get();
        if (count($userVerif) > 0) {
            throw new AuthentificationException('Cette adresse mail est déjà utilisée');
        } else {
            $user = new User();
            $user->nom = $name;
            $user->prenom = $firstname;
            $user->mail = $email;
            $user->password = $this->hashPassword($pass);
            $user->level = $level;
            $user->save();
        }
    }
}