<?php

namespace mygiftbox\control;


use mf\control\AbstractController;
use mygiftbox\model\Benefit;
use \mygiftbox\model\Category;
use mygiftbox\view\MygiftboxView;
use mf\router\Router;
use \mygiftbox\model\User;
use \mygiftbox\model\Command;
use \mygiftbox\model\ListeAchat;

class MygiftboxController extends AbstractController
{
    /* Constructeur :
         *
         * Appelle le constructeur parent
         *
         * c.f. la classe \mf\control\AbstractController
         *
         */

    public function __construct(){
        parent::__construct();
    }
    /** Méthode viewBenefits
     *
     * Réalise la fonctionnalité : afficher la liste des prestations
     *
     */
    public function viewBenefits () {
        if(isset($this->request->get['tri'])){
            $tri = $this->request->get['tri'];
        }
        else{
            $tri = "";
        }

        switch ($tri) {
            case 'ASC':
                $benefits = Benefit::orderBy('prix', 'ASC')->get();
            break;

            case 'DESC':
                $benefits = Benefit::orderBy('prix', 'DESC')->get();
            break;

            default:
                $benefits = Benefit::get();
            break;
        }
        
        $vue = new MygiftboxView($benefits);
        $vue->render('benefits');

    }


    /** Méthode viewCategories
     *
     * Réalise la fonctionnalité : afficher la liste des catégories
     *
     */
    public function viewCategories(){
        $categories = Category::get();
        $view = new MygiftboxView($categories);
        $view->render('categories');
    }




    /** Méthode viewBenefitsCategory
     * Selectionne la liste des prestation par cartégorie
     * @param int GET : id de la categorie
     * @param mixed GET : tri par pix ascendant ou descendant
      */
    public function viewBenefitsCategory(){
        if(isset($this->request->get['category'])){
            $idCategory = intval($this->request->get['category']);
            if($idCategory != 0){
                $request = Category::where('id','=',$idCategory);
                $category = $request->first();

                if(isset($this->request->get['tri'])){
                    $tri = $this->request->get['tri'];
                }
                else{
                    $tri = "";
                }
                $request = Benefit::where('idCategorie','=',$idCategory);

                switch ($tri) {
                    case 'ASC':
                        $request = $request->orderBy('prix', 'ASC');
                    break;
                    case 'DESC':
                        $request = $request->orderBy('prix', 'DESC');
                    break;
                }
                $benefits = $request->get();

                $view = new MygiftboxView(array($category, $benefits));
                $view->render('benefitsCategory');
            }
            else{
                throw new \Exception("La catégorie indiquée n'existe pas");
            }
        }
        else{
            throw new \Exception("Aucune catégorie n'est indiquée");
        }
    }

    /** Méthode viewBenefit
     *
     * Réalise la fonctionnalité : afficher la liste des catégories
     * @param int GET : id de la prestation
      */
    public function viewBenefit () {
        if(isset($this->request->get['id'])){
            $idBenefit = intval($this->request->get['id']);
            if($idBenefit != 0){
                $requete = Benefit::where('id', '=', $idBenefit);
                $benefit = $requete->first();
                if(!is_null($benefit)){
                    $view = new MygiftboxView($benefit);
                    $view->render('benefit');
                }
                else{
                    throw new \Exception("No benefit corresponds the specified id");
                }
            }
            else{
                throw new \Exception("The parameter for id isn't an integer");
            }
        }
        else{
            throw new \Exception("No id is specified");
        }
    }

    /** Méthode viewHome
     * Affichage de la page d'accueil
      */
    public function viewHome () {
        $benefits = Benefit::orderBy('id', 'ASC')->take(7)->get();
        $view = new MygiftboxView($benefits);
        $view->render('home');
    }

    /**
     * Methode viewBasketUrl
     * @param mixed GET : url de la commande
     * @param mixed GET : slide (optionnel)
      */
    public function viewUrlOffert(){
          if(isset($this->request->get['url'])){
            $url = filter_var($this->request->get['url']);

            $command = Command::where('url','=',$url)->first();

            if(!empty($command)){
                if($command->dateAcces <= date('Y-m-d')){
                    $command->idEtat = 5;
                    $command->save();
                    if(isset($this->request->get['slide'])){
                        $slide = intval($this->request->get['slide']);
                        $view = new MygiftboxView(array($command, $slide, $url));
                        $view->render('offert');
                    }
                    else{
                        $view = new MygiftboxView(array($command,$url));
                        $view->render('offertList');
                    }
                }
                else{
                    $date = date('d/m/Y', strtotime($command->dateAcces));
                    $view = new MygiftboxView("Le coffret n'est pas encore disponible, revenez le ".$date.".");
                    $view->render('dateMessage');
                }
            }
            else{
                throw new \Exception("Le coffret n'existe pas");
            }
        }
    }

    /** Methode sendMessageOffert
     * Stockage du message du destinataire
     * @param int POST id de la commande
     * @throws \Exception
      */
    public function sendMessageOffert(){
        if(isset($this->request->post['idBasket'])){
            $idBasket = filter_var($this->request->post['idBasket'], FILTER_VALIDATE_INT);
            $message = filter_var($this->request->post['message']);

            if($idBasket != false && $message != false){
                $basket = Command::where('id','=',$idBasket)->first();

                if(!empty($basket)){
                    $basket->messageDestinataire = $message;
                    $basket->save();

                    Router::executeRoute('home');
                }
                else{
                    throw new \Exception("Coffret introuvable");
                }
            }
            else{
                throw new \Exception("Le message n'est pas conforme");
            }
        }
        else{
            throw new \Exception("L'id n'est pas donné");
        }
    }
}
