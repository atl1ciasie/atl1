<?php


namespace mygiftbox\control;

use mf\auth\exception\AuthentificationException;
use mf\control\AbstractController;
use mygiftbox\model\Command;
use mygiftbox\model\ListeAchat;
use mygiftbox\view\MygiftboxView;
use mygiftbox\auth\MygiftboxAuthentification;
use mf\router\Router;
use mygiftbox\model\User;
use mygiftbox\model\Benefit;

class MygiftboxAdminController extends AbstractController {
    /* Constructeur :
         *
         * Appelle le constructeur parent
         *
         * c.f. la classe \mf\control\AbstractController
         *
         */

    public function __construct(){
        parent::__construct();
    }

    /** Méthode viewFormLogin
     * Affichage du formulaire de connexion
      */
    public function viewFormLogin(){
        $view = new MygiftboxView('');
        $view->render('login');
    }


    /** Méthode checkLogin
     * Vérifie des données passé en paramettre et connecte l'tulisateur si les données sont correctes
      */
    public function checkLogin(){
        if(isset($this->request->post['btSubmit'])){
            $email = filter_var($this->request->post['txtEmail'], FILTER_VALIDATE_EMAIL);
            $password = filter_var($this->request->post['txtPassword']);

            if(!$email){
                throw new AuthentificationException("L'email est incorrect");
            }
            if(!$password){
                throw new AuthentificationException("Le mot de passe est incorrect");
            }

            $user = new MygiftboxAuthentification();
            $router = new Router();

            if($user->loginUser($email, $password)){
                $router->executeRoute('benefits');
            }
            else{
                $router->executeRoute('formLogin');
            }
        }
    }

    /** Méthode logout
     * Déconnecte un utilisateur connecté
      */
    public function logout(){
        $user = new MygiftboxAuthentification();
        $user->logout();

        if(!$user->logged_in){
            $router = new \mf\router\Router();
            $router->executeRoute('benefits');
        }
    }

    /** Méthode viewSignup
     *
     * Réalise la fonctionnalité : afficher le formulaire d'inscription
     *
     */

    public function viewSignup () {
        $view = new \mygiftbox\view\MygiftboxView();
        $view->render('signup');
    }

    /** Méthode sendSignup
     *
     * Réalise la fonctionnalité : enregistrer dans la bdd un nouvel utilisateur
     *
     * @throws AuthentificationException
     */
    public function sendSignup() {
        $name = filter_var($this->request->post['name'] , FILTER_SANITIZE_SPECIAL_CHARS );
        $firstname = filter_var($this->request->post['firstname'] , FILTER_SANITIZE_SPECIAL_CHARS );
        $email = filter_var($this->request->post['email'] , FILTER_VALIDATE_EMAIL );
        $password = $this->request->post['password'];
        $passwordVerify = $this->request->post['password_verify'];
        if ($password === $passwordVerify) {
            $authentification = new MygiftboxAuthentification;
            $authentification->createUser($name,$firstname, $email, $password);
            Router::executeRoute('login');
        } else {
            throw new \Exception("Les mot de passe sont différents");
            Router::executeRoute('signup');
        }
    }

    /** Méthode viewBasket
     * Affichage du coffret
      */
    public function viewBasket() {
        /* $auth = new MygiftboxAuthentification();
        $user = User::where('mail', '=', $auth->user_login)->first(); */
        $benefits = array();
        $nbBenefits = array();
        if($this->createBasket()){
            $idBenefits = $_SESSION['basket']['benefit'];
            $nbBenefits = $_SESSION['basket']['nbBenefits'];
            if(!empty($idBenefits)){
                foreach($idBenefits as $id){
                    $benefits[] = Benefit::where('id','=',$id)->first();
                }
            }
        }
        $vue = new MygiftboxView(array($benefits, $nbBenefits));
        $vue->render('basket');
    }

    /**Méthode createBasket
     *
     * pour créer un panier dans une session si il n'est pas déjà créé
     *
     * @return bool // pour dire que le panier est créé
     */
    public function createBasket($idCommand=NULL){
        if (!isset($_SESSION['basket'])){
            $_SESSION['basket'] = [];
            $_SESSION['basket']['benefit'] = [];
            $_SESSION['basket']['nbBenefits'] = [];
            $_SESSION['basket']['idCommand'] = $idCommand;
            $_SESSION['basket']['idEtat'] = null;
        }
        return true;
    }

    /** Méthode addBasket
     *
     * pour ajouter des prestation dans coffret
     *
     * @throws \Exception
     */

    public function addBasket() {
        $idBenefit =filter_var($this->request->post['idBenefit'], FILTER_VALIDATE_INT);

        //Si le panier existe
        if ($this->createBasket()){
            $indexBenefit = array_search($idBenefit, $_SESSION['basket']['benefit']);
            if ($indexBenefit !== false){
                $_SESSION['basket']['nbBenefits'][$indexBenefit] +=1;
            } else {
                //Sinon on ajoute la prestation
                $_SESSION['basket']['benefit'][] = $idBenefit;
                $_SESSION['basket']['nbBenefits'][] = 1;
            }
            Router::executeRoute('benefits');

        } else {
            throw new \Exception('Un problème avec la création du panier');
        }
    }

    /** Méthode removeBasket
     *
     * pour supprimer un produit du panier
     *
     * @throws \Exception
     */

    public function removeBasket(){
        $idBenefit =filter_var($this->request->post['idBenefit'], FILTER_VALIDATE_INT);
        //Si le panier existe
        if ($this->createBasket()){
            $temp=[];
            $temp['benefit']=[];
            $temp['nbBenefits']=[];
            if(isset($_SESSION['basket']['idCommand'])){
                $temp['idCommand']=$_SESSION['basket']['idCommand'];
            }

            for($i = 0; $i < count($_SESSION['basket']['benefit']); $i++){
                if ($_SESSION['basket']['benefit'][$i] !== $idBenefit){
                    //si ça n'est pas la presta que je veux supprimer je rempli tab temporaire
                    $temp['benefit'][] = $_SESSION['basket']['benefit'][$i];
                    $temp['nbBenefits'][] = $_SESSION['basket']['nbBenefits'][$i];
                } elseif ($_SESSION['basket']['nbBenefits'][$i] > 1) {
                    // si la quantité de la prestation est supérieur à 1
                    // je ne la mets dans le tab temporaire en enlevant 1 à la quantité
                    $temp['benefit'][] = $_SESSION['basket']['benefit'][$i];
                    $temp['nbBenefits'][] = $_SESSION['basket']['nbBenefits'][$i] - 1;
                }

            }
            //On remplace le panier en session par notre panier temporaire à jour
            $_SESSION['basket'] = $temp;
            //On efface notre panier temporaire
            unset($temp);
            Router::executeRoute('basket');
        } else {
            throw new \Exception('Un problème avec le panier');
        }
    }

    /** Méthode saveBasket
     *
     * mise à jour de la commande en statut 'en cours' en session
     * et appel de l'enregistrement en bdd
     *
     * @throws Exception
     * @throws AuthentificationException
     */
    public function saveBasket() {
        $auth = new MygiftboxAuthentification();
        if ($auth->logged_in){
            if($this->createBasket()) {
                $_SESSION['basket']['idEtat'] = 1; // en cours
                $this->basketToBdd($auth);
                /*$user = User::where('mail', '=', $auth->user_login)->first();
                $newCommand = new Command();

                $newCommand->idUtilisateur = $user->id;
                $newCommand->idEtat = 1; // Met l'état de la commande à En cours de création

                $idNewCommand = $newCommand->save();
                for ($i = 0; $i < count($_SESSION['basket']['benefit']); $i++) {
                    $newlistAchat = new ListeAchat();
                    $newlistAchat->idCommande = $idNewCommand;
                    $newlistAchat->idPrestation = $_SESSION['basket']['benefit'][$i];
                    $newlistAchat->quantite = $_SESSION['basket']['nbBenefits'][$i];
                    $newlistAchat->save();
                    $_SESSION['basket']['idCommand'] = $idNewCommand;

                }*/
                Router::executeRoute('benefits');
                //Router::executeRoute('profil', [['id', $user->id]]);//??????????????????????????????????????????
            } else {
                throw new \Exception('Le panier doit être rempli pour être sauvegardé');
            }

        } else {
            throw new AuthentificationException("Problème d'authentification");
        }
    }

    /** Méthode generateUrl
     * Elle genere une chaine de caractere aleatoire
     * 
     * @return mixed chaine de caractaire aléatoire
      */
    public function generateUrl(){
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $url = '';
        for($i=0; $i<20; $i++){
            $url .= $chars[rand(0, strlen($chars)-1)];
        }
        return $url;
    }

    /** Méthode genereBasketUrl
     * Elle génere url du coffret
     * 
     * @throws Exception
      */
    public function genereBasketUrl(){
        $idBasket =filter_var($this->request->post['idBasket'], FILTER_VALIDATE_INT);
        if(!empty($idBasket)){
            $basket = Command::where('id','=',$idBasket)->first();
    
            if($basket->etat->id == 3){
                if(isset($this->request->post['btSubmit'])){
                    //Si on appui sur le bouton générer
                    do{
                        $url = $this->generateUrl();
                        $basketUrl = Command::where('url','=',$url)->first();
                    }while(!empty($basketUrl));

                    $basket->url = $url;
                    $basket->idEtat = 4;
                    $basket->save();
                }
                $view = new MygiftboxView($basket);
                $view->render('url');
            }
            else{
                throw new \Exception("La commande n'est pas payée");
            }
        }
    }

    /** Méthode basketToBdd
     *
     * sauvegarde la commande en session dans la bdd
     *
     * @param $auth
     * @return Command
     */
    public function basketToBdd ($auth) {

        $user = User::where('mail', '=', $auth->user_login)->first();

        if (isset($_SESSION['basket']['idCommand']) && $_SESSION['basket']['idCommand'] !== null) {
            // si la commande existe, on la récupere
            $newCommand = Command::find($_SESSION['basket']['idCommand']);
        } else {
            // sinon on la créé
            $newCommand = new Command();
        }

        $newCommand->idUtilisateur = $user->id;
        $newCommand->idEtat = $_SESSION['basket']['idEtat']; // Met l'état de la commande
        $newCommand->save();
        $existingListeAchats = $newCommand->listeAchats;
        // si il y a deja des prestations on les supprime avant d'enregistrer
        if( count($existingListeAchats) > 0  ) {
            foreach ($existingListeAchats as $listeAchat) {
                $listeAchat->delete();
            }
        }
        // enregistrement des nouvelles prestations
        for ($i = 0; $i < count($_SESSION['basket']['benefit']); $i++) {
            $newlistAchat = new ListeAchat();
            $newlistAchat->idCommande = $newCommand->id;
            $newlistAchat->idPrestation = $_SESSION['basket']['benefit'][$i];
            $newlistAchat->quantite = $_SESSION['basket']['nbBenefits'][$i];
            $newlistAchat->save();
            $_SESSION['basket']['idCommand'] = $newCommand->id;
        }
        return $newCommand;
    }

    /** Méthode bddToBasket
     *
     * sauvegarde la commande en bdd dans la session
     *
     * @param $idCommand
     * @return Command
     */
    public function bddToBasket($idCommand) {
        $command = Command::find($idCommand);
        $_SESSION['basket'] = [];
        $_SESSION['basket']['idCommand'] = $idCommand;
        $_SESSION['basket']['idEtat'] = $command->idEtat;
        $_SESSION['basket']['benefit'] = [];
        $_SESSION['basket']['nbBenefits'] = [];

        $listeAchats = $command->listeAchats;
        if(count($listeAchats) > 0) {
            foreach ($listeAchats as $listeAchat) {
                $_SESSION['basket']['benefit'][] = $listeAchat->idPrestation;
                $_SESSION['basket']['nbBenefits'][] = $listeAchat->quantite;
            }
        }
    }

    /** Méthode validateBasket
     *
     * Pour valider mon coffret
     *
     * @throws \Exception
     */
    public function validateBasket(){
        $auth = new MygiftboxAuthentification();
        if ($auth->logged_in){
            //$user = User::where('mail', '=', $auth->user_login)->first();
            if(isset($_SESSION['basket']['idCommand'])){
                $command = Command::find($_SESSION['basket']['idCommand']);
                $_SESSION['basket']['idEtat'] = 1;
            }

            if (empty($command)){//Si la commande n'a pas encore été sauvegardé einitialise l'état dans la session
                $_SESSION['basket']['idEtat'] = 1;// Pour l'instant on enregistre la commande à l'etat en cours(on attend d'avoir fait verif conditions avant de mettre à l'état 2 càd validé/

            }
            $command = $this->basketToBdd($auth); //enregistrement dans la base à l'etat sauvegardé
            $benefitsCommand = $command->benefits;

            $categories = []; //Tableau pour enregistrer les catégories des différentes prestations
            foreach ($benefitsCommand as $benefit){
                $category = $benefit->category;
                $categories[$category->id] = $category->nom;
            }
            if (count($categories) >= 2 && count($benefitsCommand) >= 2 ){
                //Vérification des conditions de validation d'un coffret à savoir 2 presta de 2 catèg différentes
                $_SESSION['basket']['idEtat'] = 2;
                $command->idEtat = 2;
                $command->save();

            } else {
                throw new \Exception('Le coffret doit contenir au moins 2 prestations de 2 catégories différentes');
            }

            $vue = new MygiftboxView($command);
            $vue->render('message');

        }
    }

    /**
     * methode basketReturn
     * recharge en session la commande passée en post
     * et affiche le panier
     */
    public function basketReturn () {

        if(!empty($this->request->post['id']) ) {
            $idCommand= $this->request->post['id'];
            // on recharge la commande en session
            $this->bddToBasket($idCommand);
        }
        Router::executeRoute('basket');
    }

    /** Méthode viewProfil
     * Affichage du profil de l'utilisateur
      */
    public function viewProfil(){
        $auth = new MygiftboxAuthentification();

        $email = $auth->user_login;
        $requete = User::where('mail', '=', $email);
        $user = $requete->first();
        $commands = array();
        $commands = Command::where('idUtilisateur', '=', $user->id)->orderBy('id','DESC')->get();
        $view = new MygiftboxView(array($user, $commands));
        $view->render('profil');
    }

    /** Méthode sendMessage
     * Stock en base de donnée le message du créateur du coffret
     * @param int POST id de la commande
     * @param mixed POST nom du createur
     * @param mixed POST prenom du createur
     * @param mixed POST email du createur
     * @param mixed POST date d'acces
     * @param mixed POST message du createur
      */
    public function sendMessage(){
        $idBasket = filter_var($this->request->post['idBasket']);
        $name = filter_var($this->request->post['name']);
        $firstName = filter_var($this->request->post['firstName']);
        $email = filter_var($this->request->post['mail'], FILTER_VALIDATE_EMAIL);
        $date = filter_var($this->request->post['date']);
        $message = filter_var($this->request->post['message']);

        $date = date('Y-m-d', strtotime($date));
        if (\DateTime::createFromFormat('Y-m-d', $date)){
            $basket = Command::where('id','=',$idBasket)->first();

            if(!empty($basket)){
                $basket->dateAcces = $date;
                $basket->emailMsg = $email;
                $basket->prenomMsg = $firstName;
                $basket->nomMsg = $name;
                $basket->messageCreateur = $message;
                $basket->save();

                $view = new MygiftboxView($basket);
                $view->render('goReview');
            }
        }
    }

    /** Méthode viewReview
     * Affichage du récapitulatif de la commande
      */
    public function viewReview(){
        $idBasket = filter_var($this->request->get['idBasket']);
        if(!empty($idBasket)){
            $basket = Command::where('id','=',$idBasket)->first();

            if(!empty($basket)){
                if($basket->etat->id == 2){
                    $view = new MygiftboxView($basket);
                    $view->render('review');
                }
            }
            else{
                throw new \Exception("Aucune commande ne correspont");
            }
        }
        else{
            throw new \Exception("Aunce commande passé en parametre");
        }
    }

    /** Méthode viewPay
     * Affichage du formulaire pour le paiement du coffret
      */
    public function viewPay(){
        $idBasket = filter_var($this->request->post['idBasket']);

        if(!empty($idBasket)){
            $basket = Command::where('id','=',$idBasket)->first();

            if(!empty($basket)){
                if($basket->etat->id == 2){
                    $view = new MygiftboxView($basket);
                    $view->render('pay');
                }
            }
            else{
                throw new \Exception("Aucune commande ne correspont");
            }
        }
        else{
            throw new \Exception("Aunce commande passé en parametre");
        }
    }

    /** Méthode sendPay
     * Vérifi le paiement
     * @param int POST id de la commande
      */
    public function sendPay(){
        $idBasket = filter_var($this->request->post['idBasket']);

        if(!empty($idBasket)){
            $basket = Command::where('id','=',$idBasket)->first();

            if(!empty($basket)){
                if($basket->etat->id == 2){
                    $basket->idEtat = 3;
                    $basket->save();
                    unset($_SESSION['basket']);

                    Router::executeRoute('profil');
                }
            }
            else{
                throw new \Exception("Aucune commande ne correspont");
            }
        }
        else{
            throw new \Exception("Aunce commande passé en parametre");
        }
    }

    /** Méthode viewMessageOffer
     * Affiche le message du destinataire
     * @param int POST id de la commande
      */
    public function viewMessageOffer(){
        $idCommand = filter_var($this->request->post['idBasket']);
        
        if(!empty($idCommand)){
            $command = Command::where('id','=',$idCommand)->first();

            if(!empty($command)){
                $view = new MygiftboxView($command);
                $view->render('messageOffer');
            }
            else{
                throw new \Exception("La commande n'existe pas");
            }
        }
        else{
            throw new \Exception("Aucun id pour la commande");
        }
    }
}