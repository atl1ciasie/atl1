<?php
namespace mf\utils;

class ClassLoader{
    private $prefixe;

    public function __construct($p_prefixe){
        $this->prefixe = $p_prefixe;
    }

    public function loadClass($nomClass){
        $res = $this->prefixe;
        $res .= DIRECTORY_SEPARATOR;
        $res .= str_replace("\\", DIRECTORY_SEPARATOR, $nomClass);
        $res .= ".php";
    
        if(file_exists($res)){
            require_once $res;
        }
    }

    public function register(){
        spl_autoload_register(array($this, 'loadClass'));
    }
}