<?php

namespace mf\router;

class Router extends AbstractRouter
{
    public static $routes;
    public static $aliases;
    public static $image_path;

    public function __construct()
    {
        AbstractRouter::__construct();
        self::$image_path = dirname($_SERVER['SCRIPT_NAME']).'/html/img/';
    }

    /**
     * Ajoute une route au tableau de route
     * @param string $name : alias de la route
     * @param string $url : url de la route
     * @param string $ctrl : namespace du controleur visé
     * @param string $mth : méthode du controleur visé
     * @param mixed $lvl : niveau d'acces de la route
      */
    public function addRoute($name, $url, $ctrl, $mth, $lvl){
        self::$routes[$url] = [$ctrl, $mth, $lvl];
        self::$aliases[$name] = $url;
    }

    /**
     * Attribut une route par défaut
     * @param string $url : url de la route
      */
    public function setDefaultRoute($url){
        self::$aliases['default'] = $url;
    }

    /**
     * Créé un lien ver une route
     * @param string $route_name : nom de la route
     * @param array $param_list : parametre $_GET  d'une route
     * 
     * @return mixed lien de route
      */
    public function urlFor($route_name, $param_list=[]){
        $route = $this->http_req->script_name;
        foreach (self::$aliases as $nom => $routeA) 
        {
            if($nom == $route_name)
            {
                $route .= $routeA;
            }
        }
        if(!empty($param_list)){

            for ($i=0; $i < count($param_list); $i++){ 
                if($i == 0)
                {
                    $route .= "?".$param_list[$i][0]."=".$param_list[$i][1];
                }
                else{
                    $route .= "&".$param_list[$i][0]."=".$param_list[$i][1];
                }
            }
        }
        return $route;
    }

    /**
     * Génère l'ensemble des routes et alias
     * 
     * @return mixed methode de l'instance de la route
      */
    public function run(){
        $urld = self::$aliases['default'];
        $route = self::$routes[$urld];
        $dctrl = $route[0];
        $dmth = $route[1];
        $path_info = $this->http_req->path_info;
        if (isset(self::$routes[$path_info])){
            $route = self::$routes[$path_info];
            $ctrl = $route[0];
            $mth = $route[1];
            $lvl = $route[2];
            $authentification = new \mf\auth\Authentification();
            $accessright = $authentification->checkAccessRight($lvl);
            if ($accessright ){
                $inst = new $ctrl();
                return $inst->$mth();
            }
            else{
                $inst = new \mygiftbox\control\MygiftboxAdminController();
                return $inst->viewFormLogin();
            }
        }
        $inst = new $dctrl();
        return $inst->$dmth();
    }

    /**
     * Renvoi vers une route
     * @param string $alias : alias d'une route
     * @return mixed : la méthode d'un controller
      */

    public static function executeRoute($alias){
        foreach (self::$aliases as $nom => $route){
            if($nom == $alias)
            {
                foreach (self::$routes as $key => $value){
                    if($key == $route){
                        $objet = new $value[0]();
                        $mth = $value[1];
                        $objet->$mth();
                    }
                }
            }
        }
    }
}
