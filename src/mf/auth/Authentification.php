<?php
namespace mf\auth;

class Authentification extends AbstractAuthentification{

    public function __construct(){
        if(isset($_SESSION['user_login'])){
            $this->user_login = $_SESSION['user_login'];
            $this->access_level = $_SESSION['access_level'];
            $this->logged_in = true;
        }
        else{
            $this->user_login = null;
            $this->access_level = self::ACCESS_LEVEL_NONE;
            $this->logged_in = false;
        }
    }


    /**
     * Mise a jour de la session
     * @param string $username : Nom d'utilisateur
     * @param mixed $level : Niveau d'acces de l'utilisateur
      */
    protected function updateSession($username, $level){
        $this->user_login = $username;
        $this->access_level = $level;
        
        $_SESSION['user_login'] = $username;
        $_SESSION['access_level'] = $level;

        $this->logged_in = true;
    }

    /**
     * Déconnexion de l'utilisateur
      */
    public function logout(){
        unset($_SESSION['user_login']);
        unset($_SESSION['access_level']);

        $this->user_login = null;
        $this->access_level = self::ACCESS_LEVEL_NONE;
        $this->logged_in = false;
    }

    /**
     * Vérification du niveau d'acces
     * @param mixed $requested : niveau d'acces requit
     * 
     * @return bool Si true acces autorisé, Sinon acces refusé
      */
    public function checkAccessRight($requested){
        if($requested > $this->access_level){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Connecte l'utilisateur en verifiant son mot de passe
     * @param string $username : nom d'utilisateur
     * @param string $db_pass : hash du mot de passe en base de donnée
     * @param string $given_pass : mot de passe donné par l'tulisateur
     * @param mixed $level : Niveau d'acces de l'utilisateur
     * @return bool Si true l'utilisateur est connecté, Sinon son mot de passe est incorrect
     *
     * @throws exception\AuthentificationException
      */

    public function login($username, $db_pass, $given_pass, $level){
        if($this->verifyPassword($given_pass, $db_pass)){
            $this->updateSession($username, $level);
            return true;
        }
        else{
            throw new \mf\auth\exception\AuthentificationException('Le mot de passe est incorrect');
            return false;
        }
    }

    /**
     * Hash le mot de passe
     * @param string $password : mot de passe
     * 
     * @return string mot de passe hashé
      */
    protected function hashPassword($password){
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Vérifie le mot de passe
     * @param string $password : mot de passe donné (en clair)
     * @param string $hash : mot de passe hashé
     * 
     * @return bool Si true mot de passe identique, Sinon mot de passe différent
      */
    protected function verifyPassword($password, $hash){
        return password_verify($password, $hash);
    }
}