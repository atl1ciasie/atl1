<?php
session_start();
ini_set('display_errors', 1);
require_once 'vendor/autoload.php'; /* pour le chargement automatique des classes dans vendor */
require_once 'src/mf/utils/ClassLoader.php'; /* Chargement des classes */

$loader = new \mf\utils\ClassLoader('src');
$loader->register();

use mf\view\AbstractView;
use mf\router\Router;
use mygiftbox\auth\MygiftboxAuthentification;
use mygiftbox\view\MygiftboxView;

AbstractView::addStyleSheet('html/css/style.css');

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();
$configFile = "conf/config.ini";
if(file_exists("conf/config.local.ini")) {
    $configFile = "conf/config.local.ini";
}

$db->addConnection(parse_ini_file($configFile)); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

$router = new Router();

//Route pour afficher la liste des prestations
$router->addRoute('benefits',
                  '/benefits/',
                  '\mygiftbox\control\MygiftboxController',
                  'viewBenefits',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);

//Route pour afficher la liste des categories
$router->addRoute('categories',
                  '/category/',
                  '\mygiftbox\control\MygiftboxController',
                  'viewCategories',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);

//Route pour afficher le formulaire d'inscription
$router->addRoute('signup',
                  '/signup/',
                  '\mygiftbox\control\MygiftboxAdminController',
                  'viewsignup',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);

//Route pour enregistrer dans la base une inscription
$router->addRoute('sendSignup',
                  '/sendSignup/',
                  '\mygiftbox\control\MygiftboxAdminController',
                  'sendSignup',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);

//Route pour les prestation d'une catégorie choisie
$router->addRoute('benefitsCategory',
                  '/category/benefit/',
                  '\mygiftbox\control\MygiftboxController',
                  'viewBenefitsCategory',
                  MygiftboxAuthentification::ACCESS_LEVEL_NONE
                );

//Route pour afficher le formulaire de connexion
$router->addRoute('login',
                  '/login/',
                  '\mygiftbox\control\MygiftboxAdminController',
                  'viewFormLogin',
                  MygiftboxAuthentification::ACCESS_LEVEL_NONE
                );

//Route pour vérifier l'authentification
$router->addRoute('sendLogin',
                  '/sendLogin/',
                  '\mygiftbox\control\MygiftboxAdminController',
                  'checkLogin',
                  MygiftboxAuthentification::ACCESS_LEVEL_NONE
                );
//Route pour déconnecté un utilisateur
$router->addRoute('logout',
                  '/logout/',
                  '\mygiftbox\control\MygiftboxAdminController',
                  'logout',
                  MygiftboxAuthentification::ACCESS_LEVEL_USER
                );

//Route pour afficher une prestation
$router->addRoute('benefit',
                  '/benefit/',
                  '\mygiftbox\control\MygiftboxController',
                  'viewBenefit',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE
                );


//Route pour afficher mon coffret (panier)
$router->addRoute('basket',
                '/basket/',
                '\mygiftbox\control\MygiftboxAdminController',
                'viewBasket',
                MygiftboxAuthentification::ACCESS_LEVEL_USER);

//Route pour ajouter une prestation à mon coffret
$router->addRoute('addBasket',
                '/addBasket/',
                '\mygiftbox\control\MygiftboxAdminController',
                'addBasket',
                MygiftboxAuthentification::ACCESS_LEVEL_USER);

//Route pour supprimer une prestation à mon coffret
$router->addRoute('removeBasket',
                '/basket/removeBeneﬁt/',
                '\mygiftbox\control\MygiftboxAdminController',
                'removeBasket',
                MygiftboxAuthentification::ACCESS_LEVEL_USER);



//Route pour sauvegarder mon coffret
$router->addRoute('saveBasket',
    '/saveBasket/',
    '\mygiftbox\control\MygiftboxAdminController',
    'saveBasket',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);

//Route pour afficher la page d'accueil
$router->addRoute('home',
                  '/home/',
                  '\mygiftbox\control\MygiftboxController',
                  'viewHome',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE
                );


//Route pour valider mon coffret et rentrer un message pour destinataire
$router->addRoute('profil',
                  '/profil/',
                  '\mygiftbox\control\MygiftboxAdminController',
                  'viewProfil',
                  \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_USER
                );

//Route pour valider mon coffret
$router->addRoute('basketMessage',
    '/basket/message/',
    '\mygiftbox\control\MygiftboxAdminController',
    'validateBasket',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);
//Route pour générer l'url du coffret
$router->addRoute('basketUrl',
                '/basket/url/',
                '\mygiftbox\control\MygiftboxAdminController',
                'genereBasketUrl',
                \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_USER
            );

//Ajout de la route pour valider mon coffret
$router->addRoute('basketReturn',
    '/basket/return/',
    '\mygiftbox\control\MygiftboxAdminController',
    'basketReturn',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);

//Envoi du message de la commande
$router->addRoute('sendMessage',
    '/basket/sendMessage/',
    '\mygiftbox\control\MygiftboxAdminController',
    'sendMessage',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);
//Route de récapitulatif du coffret
$router->addRoute('reviewBasket',
    '/basket/review/',
    '\mygiftbox\control\MygiftboxAdminController',
    'viewReview',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);
//Route pour afficher le formulaire de paiement
$router->addRoute('viewPay',
    '/basket/pay/',
    '\mygiftbox\control\MygiftboxAdminController',
    'viewPay',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);
//Route pour vérifier le paiement
$router->addRoute('sendPay',
    '/sendBasket/',
    '\mygiftbox\control\MygiftboxAdminController',
    'sendPay',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);
//Route afficher le message du destinataire
$router->addRoute('messageOffer',
    '/messageOffer/',
    '\mygiftbox\control\MygiftboxAdminController',
    'viewMessageOffer',
    MygiftboxAuthentification::ACCESS_LEVEL_USER);
//Affichage pour le destinataire d'une prestation de son cadeau 
$router->addRoute('urlOffert',
                '/url/',
                '\mygiftbox\control\MygiftboxController',
                'viewUrlOffert',
                \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE
            );
//Route pour enregistrer le message du destinataire
$router->addRoute('sendMessageOffert',
                '/url/sendMessage/',
                '\mygiftbox\control\MygiftboxController',
                'sendMessageOffert',
                \mygiftbox\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE
            );
//Route par défaut
$router->setDefaultRoute('/home/');

try {
    $router->run();

} catch (Exception $e) {
    $view = new MygiftboxView($e->getMessage());
    $view->render('error');
}