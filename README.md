Pour installer l'application sur le serveur :

1. Aller dans le dossier qui va contenir l'application sur le serveur webetu

2. Cloner l'application en ouvrant un terminal à partir de ce dossier et entrer la commande suivante :
	git clone git@bitbucket.org:atl1ciasie/atl1.git

3. S'assurer que Composer est à jour avec la version de PHP qui convient, toujours dans le terminal et dans le même dossier, avec cette commande :
	composer update	|(la 'version' utilisée sur webetu est la 5.5.* telle qu'affichée dans le fichier composer.json) |(la version de php sur webetu est la 7.0.32)

4. Importer le fichier "Script_BD_Données&Structure.sql" dans phpMyAdmin pour créer la base de données (le script ne contient pas la création de la base, seules les tables et les données)

5. Se rendre dans le fichier config.ini et y passer le bon nom de la base de données (qui devrait être similaire au nom d'utilisateur), le nom d'utilisateur et le mot de passe de phpMyAdmin :
	database = <database>	| = <username>
	username = <username>
	password = <password>

6. Se rendre à l'adresse correspondante au nom d'utilisateur sur webetu (modifier <username>) :
	https://webetu.iutnc.univ-lorraine.fr/www/<username>/atl1/main.php

7. Bienvenue dans l'application ! Il reste quelques détails à prendre en compte :

Le mot de passe des utlisateurs correspond à leur prénom (sans caractères spécifiques, pas d'accent, etc...), voici une liste non exhaustive des utilisateurs :

<nom>		<prénom>	<mail>			<mdp>
Lacroix		Amélie		amelie.lacroix@mail.fr	amelie
McCree		Jesse		jesse.mccree@mail.fr	jesse
Smith		John		john.smith@mail.com	john

Lien vers le talbeau de développement Trello :
https://trello.com/b/Ov2SGjIa/d%C3%A9veloppement

Lien vers le talbeau de conception Trello :
https://trello.com/b/U5mb3Nrg/conception-de-lapplication

Lien vers Bitbucket :
https://bitbucket.org/atl1ciasie/atl1/src/master/
