-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  sam. 10 nov. 2018 à 11:25
-- Version du serveur :  5.7.23-0ubuntu0.16.04.1
-- Version de PHP :  7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


--
-- Base de données :  `mygiftbox`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Restauration'),
(2, 'Hébergement'),
(3, 'Attention'),
(4, 'Activité');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `dateAcces` date DEFAULT NULL,
  `emailMsg` varchar(50) DEFAULT NULL,
  `prenomMsg` varchar(50) DEFAULT NULL,
  `nomMsg` varchar(50) DEFAULT NULL,
  `messageCreateur` varchar(255) DEFAULT NULL,
  `messageDestinataire` varchar(255) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `idEtat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `dateAcces`, `emailMsg`, `prenomMsg`, `nomMsg`, `messageCreateur`, `messageDestinataire`, `url`, `idUtilisateur`, `idEtat`) VALUES
(2, '2018-11-23', 'amelie.lacroix@mail.fr', 'Amélie', 'Lacroix', '', NULL, 'sVQzegd3jyAPwIotRMb2', 1, 4),
(3, '2018-10-11', 'amelie.lacroix@mail.fr', 'Amélie', 'Lacroix', 'cc', 'salut', 'iq4V4kV4ntvDd2o8Phfy', 1, 5),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2),
(5, '2018-08-11', 'amelie.lacroix@mail.fr', 'Amélie', 'Lacroix', 'AHAHA', NULL, 'mPoHce8xqsV4EU4EZAdG', 1, 5),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(7, '1970-01-01', 'amelie.lacroix@mail.fr', 'Amélie', 'Lacroix', '', NULL, 'k3LC2ooEi3rBy1LHBGPs', 1, 5),
(8, '2018-11-23', 'amelie.lacroix@mail.fr', 'Amélie', 'Lacroix', '', NULL, 'TLtr5yCZjBCC1Gx61sao', 1, 4),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(10, '2018-11-24', 'amelie.lacroix@mail.fr', 'Amélie', 'Lacroix', 'AHAAHAHHAHA', NULL, NULL, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE `etat` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`id`, `label`) VALUES
(1, 'En cours de création'),
(2, 'Validé'),
(3, 'Payé'),
(4, 'Transmis au destinataire'),
(5, 'Ouvert par le destinataire');

-- --------------------------------------------------------

--
-- Structure de la table `listeachat`
--

CREATE TABLE `listeachat` (
  `id` int(11) NOT NULL,
  `idCommande` int(11) NOT NULL,
  `idPrestation` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `listeachat`
--

INSERT INTO `listeachat` (`id`, `idCommande`, `idPrestation`, `quantite`) VALUES
(29, 4, 1, 1),
(30, 4, 5, 1),
(31, 4, 11, 1),
(46, 5, 1, 2),
(47, 5, 3, 1),
(48, 5, 11, 1),
(49, 3, 1, 1),
(50, 3, 13, 1),
(51, 7, 17, 1),
(52, 7, 21, 1),
(53, 7, 3, 1),
(58, 8, 5, 1),
(59, 8, 10, 1),
(60, 2, 3, 1),
(61, 2, 8, 1),
(74, 10, 2, 1),
(75, 10, 6, 1),
(76, 10, 1, 1),
(77, 10, 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

CREATE TABLE `prestation` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `lieu` text NOT NULL,
  `description` text NOT NULL,
  `prix` decimal(8,2) NOT NULL,
  `image` text NOT NULL,
  `idCategorie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `prestation`
--

INSERT INTO `prestation` (`id`, `nom`, `lieu`, `description`, `prix`, `image`, `idCategorie`) VALUES
(1, 'Champagne', 'Paris', 'Bouteille de champagne + flutes + jeux à gratter', '20.00', 'champagne.jpg', 1),
(2, 'Musique', 'Nancy', 'Partitions de piano à 4 mains', '25.00', 'musique.jpg', 1),
(3, 'Exposition', 'Paris', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', '14.00', 'poirelregarder.jpg', 2),
(4, 'Goûter', 'Nancy', 'Goûter au FIFNL', '20.00', 'gouter.jpg', 3),
(5, 'Projection', 'Paris', 'Projection courts-métrages au FIFNL', '10.00', 'film.jpg', 2),
(6, 'Bouquet', 'Nancy', 'Bouquet de roses et Mots de Marion Renaud', '16.00', 'rose.jpg', 1),
(7, 'Diner Stanislas', 'Paris', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', '60.00', 'bonroi.jpg', 3),
(8, 'Origami', 'Paris', 'Baguettes magiques en Origami en buvant un thé', '12.00', 'origami.jpg', 3),
(9, 'Livres', 'Nancy', 'Livre bricolage avec petits-enfants + Roman', '24.00', 'bricolage.jpg', 1),
(10, 'Diner  Grand Rue ', 'Paris', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', '59.00', 'grandrue.jpg', 3),
(11, 'Visite guidée', 'Nancy', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', '11.00', 'place.jpg', 2),
(12, 'Bijoux', 'Paris', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', '29.00', 'bijoux.jpg', 1),
(13, 'Opéra', 'Paris', 'Concert commenté à l’Opéra', '15.00', 'opera.jpg', 2),
(14, 'Thé Hotel de la reine', 'Paris', 'Thé de debriefing au bar de l’Hotel de la reine', '5.00', 'hotelreine.gif', 3),
(15, 'Jeu connaissance', 'Paris', 'Jeu pour faire connaissance', '6.00', 'connaissance.jpg', 2),
(16, 'Diner', 'Nancy', 'Diner (Apéritif / Plat / Vin / Dessert / Café)', '40.00', 'diner.jpg', 3),
(17, 'Cadeaux individuels', 'Paris', 'Cadeaux individuels sur le thème de la soirée', '13.00', 'cadeaux.jpg', 1),
(18, 'Animation', 'Paris', 'Activité animée par un intervenant extérieur', '9.00', 'animateur.jpg', 2),
(19, 'Jeu contacts', 'Nancy', 'Jeu pour échange de contacts', '5.00', 'contact.png', 2),
(20, 'Cocktail', 'Paris', 'Cocktail de fin de soirée', '12.00', 'cocktail.jpg', 3),
(21, 'Star Wars', 'Paris', 'Star Wars - Le Réveil de la Force. Séance cinéma 3D', '12.00', 'starwars.jpg', 2),
(22, 'Concert', 'Paris', 'Un concert à Nancy', '17.00', 'concert.jpg', 2),
(23, 'Appart Hotel', 'Nancy', 'Appart’hôtel Coeur de Ville, en plein centre-ville', '56.00', 'apparthotel.jpg', 4),
(24, 'Hôtel d\'Haussonville', 'Nancy', 'Hôtel d\'Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', '169.00', 'hotel_haussonville_logo.jpg', 4),
(25, 'Boite de nuit', 'Paris', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', '32.00', 'boitedenuit.jpg', 2),
(26, 'Planètes Laser', 'Paris', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', '15.00', 'laser.jpg', 2),
(27, 'Fort Aventure', 'Paris', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l\'élastique inversé, Toboggan géant... et bien plus encore.', '25.00', 'fort.jpg', 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `mail`, `password`, `level`) VALUES
(1, 'Lacroix', 'Amélie', 'amelie.lacroix@mail.fr', '$2y$10$XFLzq0N8nuOdHhCUuYVVze0a2a8xTARRcjtFOSs3Ix3yNVIEA6Yn.', 100),
(3, 'McCree', 'Jesse', 'jesse.mccree@mail.fr', '$2y$10$9LLoBsbm1maKqJiA5e9k4.PqEymXMJim/SRfRhoukD5HK2arjY3.O', 100),
(4, 'Smith', 'John', 'john.smith@mail.com', '$2y$10$ElUhUfCvI.jR432bjndabe6apQXo3HxbJCVAJKvJNnFZzb7v/6eCy', 100);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUtilisateur` (`idUtilisateur`),
  ADD KEY `idEtat` (`idEtat`);

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `listeachat`
--
ALTER TABLE `listeachat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCommande` (`idCommande`),
  ADD KEY `idPrestation` (`idPrestation`);

--
-- Index pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCategorie` (`idCategorie`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `etat`
--
ALTER TABLE `etat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `listeachat`
--
ALTER TABLE `listeachat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT pour la table `prestation`
--
ALTER TABLE `prestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `commande_ibfk_2` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`id`);

--
-- Contraintes pour la table `listeachat`
--
ALTER TABLE `listeachat`
  ADD CONSTRAINT `listeachat_ibfk_1` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`id`),
  ADD CONSTRAINT `listeachat_ibfk_2` FOREIGN KEY (`idPrestation`) REFERENCES `prestation` (`id`);

--
-- Contraintes pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD CONSTRAINT `prestation_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categorie` (`id`);
COMMIT;
